from flask import Flask, request, jsonify
from itsdangerous import json

app = Flask(__name__)

@app.route('/tarea1-resta-201404106', methods =["POST"])
def get_numeros():
    Entrada = request.get_json()
    a = Entrada["num1"]
    b = Entrada["num2"]
    response = {"El resutlado es ": a-b}
    return jsonify(response)
    
@app.route('/')
def hello_world():
    return 'Hello World Nueva'

if __name__ == '__main__':
    app.run(debug=True, port=3000)