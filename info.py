from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/tarea1-info-201404106', methods =["GET"])
def get_Información():
    return 'Fernando Alberto Ambrosio Alemán - 201404106'
    
@app.route('/')
def hello_world():
    return 'Hello World Nueva'

if __name__ == '__main__':
    app.run(debug=True, port=4000)